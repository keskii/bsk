package tdd.training.bsk;

public class Game {

	Frame[] gameFrames;
	int frameCounter;
	int firstBonusThr;
	int secondBonusThr;
	/**
	 * It initializes an empty bowling game.
	 */
	public Game() {
		frameCounter = 0;
		gameFrames = new Frame [10];
		
	}

	/**
	 * It adds a frame to this game.
	 * 
	 * @param frame The frame.
	 * @throws BowlingException
	 */
	public void addFrame(Frame frame) {
		gameFrames[frameCounter] = frame;
		frameCounter++;
	}

	/**
	 * It return the i-th frame of this game.
	 * 
	 * @param index The index (ranging from 0 to 9) of the frame.
	 * @return The i-th frame.
	 * @throws BowlingException
	 */
	public Frame getFrameAt(int index) {
		return gameFrames[index];	
	}

	/**
	 * It sets the first bonus throw of this game.
	 * 
	 * @param firstBonusThrow The first bonus throw.
	 * @throws BowlingException
	 */
	public void setFirstBonusThrow(int firstBonusThrow) {
		firstBonusThr = firstBonusThrow;
	}

	/**
	 * It sets the second bonus throw of this game.
	 * 
	 * @param secondBonusThrow The second bonus throw.
	 * @throws BowlingException
	 */
	public void setSecondBonusThrow(int secondBonusThrow) {
		secondBonusThr = secondBonusThrow;
	}

	/**
	 * It returns the first bonus throw of this game.
	 * 
	 * @return The first bonus throw.
	 */
	public int getFirstBonusThrow() {
		return firstBonusThr;
	}

	/**
	 * It returns the second bonus throw of this game.
	 * 
	 * @return The second bonus throw.
	 */
	public int getSecondBonusThrow() {
		return secondBonusThr;
	}

	/**
	 * It returns the score of this game.
	 * 
	 * @return The score of this game.
	 * @throws BowlingException
	 */
	public int calculateScore() {
		int gameScore = 0;
		for(int i = 0; i < 10; i++) {
			spareCalculations(i);
			strikeCalculations(i);
			gameScore += gameFrames[i].getScore();
		}
		return gameScore;	
	}
	
	private void spareCalculations(int i) {
		if(gameFrames[i].isSpare()) {
			if(i == 9) {
				gameFrames[i].setBonus(firstBonusThr);
			} else {
				gameFrames[i].setBonus(gameFrames[i+1].getFirstThrow());
			}
		}
	}
	
	private void strikeCalculations(int i) {
		if(gameFrames[i].isStrike()) {
			if(i == 9) {
				gameFrames[i].setBonus(firstBonusThr + secondBonusThr);
			} else {
				if(gameFrames[i+1].isStrike()) {
					if(i+1 == 9)
					{
						gameFrames[i].setBonus(gameFrames[i+1].getFirstThrow() + firstBonusThr);
					} else {
					gameFrames[i].setBonus(gameFrames[i+1].getFirstThrow() + gameFrames[i+2].getFirstThrow());
					}
				} else {
					gameFrames[i].setBonus(gameFrames[i+1].getFirstThrow() + gameFrames[i+1].getSecondThrow());
				}
			}
		}
	}

}
