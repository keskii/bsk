package bsk;

import static org.junit.Assert.*;

import org.junit.Test;

import tdd.training.bsk.*;


public class FrameTest {

	@Test
	public void testFirstThrowShouldBeTwo() {
		Frame frame = new Frame(2, 4);
		
		assertEquals(2, frame.getFirstThrow());
	}
	
	@Test
	public void testSecondThrowShouldBeFour() {
		Frame frame = new Frame(2, 4);
		
		assertEquals(4, frame.getSecondThrow());
	}
	
	@Test
	public void testScoreFirstThrowTwoSecondThrowSixShouldBeEight() {
		Frame frame = new Frame(2, 6);
		
		assertEquals(8, frame.getScore());
	}
	
	@Test
	public void testFirstThrowFrameAtPositionZeroShouldBeOne() {
		Game game = new Game();

		game.addFrame(new Frame(1, 5));
		game.addFrame(new Frame(2, 5));
		game.addFrame(new Frame(1, 1));
		game.addFrame(new Frame(4, 2));
		game.addFrame(new Frame(8, 0));
		game.addFrame(new Frame(2, 3));
		game.addFrame(new Frame(1, 3));
		game.addFrame(new Frame(1, 6));
		game.addFrame(new Frame(2, 0));
		game.addFrame(new Frame(10, 0));
		
		Frame expectedFrame = new Frame(1, 5);
		Frame actualFrame = game.getFrameAt(0);
		
		assertEquals(expectedFrame.getFirstThrow(), actualFrame.getFirstThrow());
	}
	
	@Test
	public void testSecondThrowFrameAtPositionZeroShouldBeFive() {
		Game game = new Game();

		game.addFrame(new Frame(1, 5));
		game.addFrame(new Frame(2, 5));
		game.addFrame(new Frame(1, 1));
		game.addFrame(new Frame(4, 2));
		game.addFrame(new Frame(8, 0));
		game.addFrame(new Frame(2, 3));
		game.addFrame(new Frame(1, 3));
		game.addFrame(new Frame(1, 6));
		game.addFrame(new Frame(2, 0));
		game.addFrame(new Frame(10, 0));
		
		Frame expectedFrame = new Frame(1, 5);
		Frame actualFrame = game.getFrameAt(0);
		
		assertEquals(expectedFrame.getSecondThrow(), actualFrame.getSecondThrow());
	}
	
	@Test
	public void testGameScoreShouldBe57() {
		Game game = new Game();

		game.addFrame(new Frame(1, 5));
		game.addFrame(new Frame(2, 5));
		game.addFrame(new Frame(1, 1));
		game.addFrame(new Frame(4, 2));
		game.addFrame(new Frame(8, 0));
		game.addFrame(new Frame(2, 3));
		game.addFrame(new Frame(1, 3));
		game.addFrame(new Frame(1, 6));
		game.addFrame(new Frame(2, 0));
		game.addFrame(new Frame(10, 0));
		
		assertEquals(57, game.calculateScore());
	}
	
	@Test
	public void testIsSpare() {
		Frame frame = new Frame(1, 9);
		
		assertTrue(frame.isSpare());
	}
	
	@Test
	public void testFirstFrameBonusShouldBeThree() {
		Frame frame1 = new Frame(1, 9);
		Frame frame2 = new Frame(3, 6);

		frame1.setBonus(frame2.getFirstThrow());
		
		assertEquals(3, frame1.getBonus());
	}
	
	@Test
	public void testFirstFrameScoreWithSpareShouldBe13() {
		Frame frame1 = new Frame(1, 9);
		Frame frame2 = new Frame(3, 6);
		
		frame1.setBonus(frame2.getFirstThrow());
		
		assertEquals(13, frame1.getScore());
	}
	
	@Test
	public void testGameScoreWithBonusShouldBe88() {
		Game game = new Game();

		game.addFrame(new Frame(1, 9));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(7, 2));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(4, 4));
		game.addFrame(new Frame(5, 3));
		game.addFrame(new Frame(3, 3));
		game.addFrame(new Frame(4, 5));
		game.addFrame(new Frame(8, 1));
		game.addFrame(new Frame(2, 6));
		
		assertEquals(88, game.calculateScore());
	}
	
	@Test
	public void testIsStrike() {
		Frame frame = new Frame(10, 0);
		
		assertTrue(frame.isStrike());
	}
	
	@Test
	public void testStrikeFrameScoreShouldBe19() {
		Frame frame1 = new Frame(10, 0);
		Frame frame2 = new Frame(3, 6);
		
		frame1.setBonus(frame2.getFirstThrow() + frame2.getSecondThrow());
		
		assertEquals(19, frame1.getScore());
	}
	
	@Test
	public void testGameScoreWithOneStrikeShouldBe94() {
		Game game = new Game();

		game.addFrame(new Frame(10, 0));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(7, 2));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(4, 4));
		game.addFrame(new Frame(5, 3));
		game.addFrame(new Frame(3, 3));
		game.addFrame(new Frame(4, 5));
		game.addFrame(new Frame(8, 1));
		game.addFrame(new Frame(2, 6));
		
		assertEquals(94, game.calculateScore());
	}
	
	@Test
	public void testGameScoreWithOneStrikeAndOneSpareConsecutiveShouldBe103() {
		Game game = new Game();

		game.addFrame(new Frame(10, 0));
		game.addFrame(new Frame(4, 6));
		game.addFrame(new Frame(7, 2));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(4, 4));
		game.addFrame(new Frame(5, 3));
		game.addFrame(new Frame(3, 3));
		game.addFrame(new Frame(4, 5));
		game.addFrame(new Frame(8, 1));
		game.addFrame(new Frame(2, 6));
		
		assertEquals(103, game.calculateScore());
	}
	
	@Test
	public void testGameScoreWithTwoConsecutiveStrikesShouldBe112() {
		Game game = new Game();

		game.addFrame(new Frame(10, 0));
		game.addFrame(new Frame(10, 0));
		game.addFrame(new Frame(7, 2));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(4, 4));
		game.addFrame(new Frame(5, 3));
		game.addFrame(new Frame(3, 3));
		game.addFrame(new Frame(4, 5));
		game.addFrame(new Frame(8, 1));
		game.addFrame(new Frame(2, 6));
		
		assertEquals(112, game.calculateScore());
	}
	
	@Test
	public void testGameScoreWithTwoConsecutiveSparesShouldBe98() {
		Game game = new Game();

		game.addFrame(new Frame(8, 2));
		game.addFrame(new Frame(5, 5));
		game.addFrame(new Frame(7, 2));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(4, 4));
		game.addFrame(new Frame(5, 3));
		game.addFrame(new Frame(3, 3));
		game.addFrame(new Frame(4, 5));
		game.addFrame(new Frame(8, 1));
		game.addFrame(new Frame(2, 6));
		
		assertEquals(98, game.calculateScore());
	}
	
	@Test
	public void testFirstBonusThrowShouldBe7() {
		Game game = new Game();
		
		game.setFirstBonusThrow(7);
		
		assertEquals(7, game.getFirstBonusThrow());
	}
	
	@Test
	public void testGameScoreWithLastFrameSpareShouldBe90() {
		Game game = new Game();

		game.addFrame(new Frame(1, 5));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(7, 2));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(4, 4));
		game.addFrame(new Frame(5, 3));
		game.addFrame(new Frame(3, 3));
		game.addFrame(new Frame(4, 5));
		game.addFrame(new Frame(8, 1));
		game.addFrame(new Frame(2, 8));
		
		game.setFirstBonusThrow(7);
		
		assertEquals(90, game.calculateScore());
	}
	
	@Test
	public void testSecondBonusThrowShouldBe2() {
		Game game = new Game();
		
		game.setSecondBonusThrow(2);
		
		assertEquals(2, game.getSecondBonusThrow());
	}
	
	@Test
	public void testGameScoreWithLastFrameStrikeShouldBe92() {
		Game game = new Game();

		game.addFrame(new Frame(1, 5));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(7, 2));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(4, 4));
		game.addFrame(new Frame(5, 3));
		game.addFrame(new Frame(3, 3));
		game.addFrame(new Frame(4, 5));
		game.addFrame(new Frame(8, 1));
		game.addFrame(new Frame(10,0));
		
		game.setFirstBonusThrow(7);
		game.setSecondBonusThrow(2);
		
		assertEquals(92, game.calculateScore());
	}
	
	@Test
	public void testGameScoreOfPerfectGameShouldBe300() {
		Game game = new Game();

		game.addFrame(new Frame(10, 0));
		game.addFrame(new Frame(10, 0));
		game.addFrame(new Frame(10, 0));
		game.addFrame(new Frame(10, 0));
		game.addFrame(new Frame(10, 0));
		game.addFrame(new Frame(10, 0));
		game.addFrame(new Frame(10, 0));
		game.addFrame(new Frame(10, 0));
		game.addFrame(new Frame(10, 0));
		game.addFrame(new Frame(10, 0));
		
		game.setFirstBonusThrow(10);
		game.setSecondBonusThrow(10);
		
		assertEquals(300, game.calculateScore());
	}

}
